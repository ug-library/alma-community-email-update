import configparser
import os
import sys
import logging.config
import requests
from datetime import datetime
import pytz
import time
import json

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

# configure a bit of logging 
alma_email_info_file = f"{BASE_DIR}{os.sep}logs{os.sep}alma_email_info.log"
exception_log_file = f"{BASE_DIR}{os.sep}logs{os.sep}exceptions.log"

LOGGING_CONFIG = None
LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'standard': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'null': {
            'level':'DEBUG',
            'class':'logging.NullHandler',
        },                
        'alma_email_rotating_file': {
            'level' : 'INFO',
            'formatter' : 'standard',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : alma_email_info_file,
            'maxBytes' : 10000000,
            'backupCount' : 7,
        },          
        'exception_rotating_file': {
            'level' : 'INFO',
            'formatter' : 'standard',
            'class' : 'logging.handlers.RotatingFileHandler',
            'filename' : exception_log_file,
            'maxBytes' : 4000000,
            'backupCount' : 5,
        },          
        'console':{
            'level':'INFO',
            'class':'logging.StreamHandler',
            'formatter': 'standard'
        },
    },
    'loggers': {        
        'alma_email_logger': {
            'handlers': ['alma_email_rotating_file'],
            'level': 'INFO',
            'propagate': True,
        },        
        'exception_logger': {
            'handlers': ['exception_rotating_file'],
            'level': 'INFO',
            'propagate': True,
        },        
    }
}

# use a customTime for logging, ensure timestamps make sense within a container
def customTime(*args):
    '''
    setup logging timestamp to be eastern time
    '''
    utc_dt = pytz.utc.localize(datetime.utcnow())
    my_tz = pytz.timezone("US/Eastern")
    converted = utc_dt.astimezone(my_tz)
    return converted.timetuple()

logging.Formatter.converter = customTime

logging.config.dictConfig(LOGGING)

# instantiate logging
# load the logger this process will use
logger = logging.getLogger('alma_email_logger')
exception_logger = logging.getLogger('exception_logger')

# start with a new log file instead of continuous roll...more for dev / unique runs
# typically will start logging in the .1 file
handler = logging.handlers.RotatingFileHandler(alma_email_info_file, mode='w', backupCount=7)
handler.doRollover()

handler = logging.handlers.RotatingFileHandler(exception_log_file, mode='w', backupCount=5)
handler.doRollover()

try:
    config = configparser.ConfigParser()
    fpath = f"{BASE_DIR}{os.sep}alma_email_ident.ini"
    config.read(fpath)

    api_host = config['api']['api_host']
    alma_user_api_key = config['api']['users_api_key']

except Exception as e:
    exception_logger.exception(e)    


def get_Patron_Record(barcode):
    '''
    Get the Alma patron record for the barcode passed in
    '''
    result = {}
    api_user_get = f"/almaws/v1/users/{barcode}?user_id_type=all_unique&view=full&expand=none"        
    headers = {'accept': 'application/json'}

    api_get_call = f"{api_host}{api_user_get}&apikey={alma_user_api_key}"
    
    r = requests.get(api_get_call, headers=headers)
    if r.status_code == 200:
        result = r.json()
    else:
        logger.info(f"PATRON DOESN'T EXIST: Barcode({barcode})")
    
    return result

def has_email_an_identifier(rec, email):
    '''
    Parse the patron record and check if the email exists as an identifier.
    Return True if the email is in the identifier list, False if not found
    Return: True/False
    '''
    has_email_ident = False
    try:
        user_ident_list = rec.get("user_identifier", [])
        for ident in user_ident_list:
            val = ident.get("value","")
            if val == email:        
                # found a match, stop looking
                has_email_ident = True
                break
    except Exception as e:        
        exception_logger.exception(e)     
    
    return has_email_ident

def update_identifier(barcode, rec, email):
    '''
    Insert the email as an identifier and perform an API PUT command to update the record
    return True if update a success, False if failed
    Return: True/False
    '''

    has_email_ident = False
    try:
        api_user_put = f"/almaws/v1/users/{barcode}?user_id_type=all_unique&send_pin_number_letter=false"
        put_headers = {'accept': 'application/json', 'Content-Type': 'application/json'}   
        api_put_call = f"{api_host}{api_user_put}&apikey={alma_user_api_key}"

        user_ident_list = rec.get("user_identifier", [])
        new_id_type = {}
        new_id_type["desc"] = "Institution ID"
        new_id_type["value"] = "INST_ID"
        new_ident = {}
        new_ident["id_type"] = new_id_type
        new_ident["note"] = "added via api at go live for email/sso auth"
        new_ident["segment_type"] = "External"
        new_ident["status"] = "Active"
        new_ident["value"] = email

        # update the identifier list
        user_ident_list.append(new_ident)
        rec["user_identifier"] = user_ident_list
        # will it update without user roles - yes, so we'll remove troubled fields
        rec.pop('user_role', None)
        rec.pop('user_title', None)

        r = requests.put(api_put_call, headers=put_headers, data=json.dumps(rec))

        # perform the PUT command
        if r.status_code == 200:            
            has_email_ident = True
        else:
            has_email_ident = False

    except Exception as e:
        exception_logger.exception(e)     
    
    return has_email_ident    

def process_file(csv_path,fname, show_list, csv_tmp_output):
    '''
    Actually read through the mapping file and perform the patron record grab, check to see if the identifier
    already exists, if it does not, then perform an update 
    '''       
    record_count = 0
    failed_count = 0
    email_list = []
    try:                        
        # create the tmp working file
        if show_list:
            tmp_path = f"{csv_tmp_output}{fname.split('.')[0]}_alma_identifier_upd_tmp.csv"

            # create an empty temp file to use, this will replace/full overwrite existing
            with open(tmp_path, "w") as tmpfile:
                tmpfile.write(f"USERNAME,Preferred Email\n")
                tmpfile.flush()

        # process the input file
        fpath = f"{csv_path}{fname}"
        with open(fpath) as f:        
            # strip off the header
            header = f.readline()                   
            for line in f:                       
                # info list is: barcode, email
                info_list = line.split(",")
                barcode = ""
                email = ""
                try:
                    barcode = info_list[0].strip()
                except Exception as e:
                        barcode = ""
                        exception_logger.exception(e)                    
                try:
                    email = info_list[1].strip()
                except Exception as e:
                        email = ""
                        exception_logger.exception(e)
                
                # Get the patron record
                if len(barcode) > 0:
                    if len(email) > 0:
                        prec = get_Patron_Record(barcode)
                        
                        if len(prec) == 0:
                            # patron record not found, skip
                            continue
                        has_email_ident = has_email_an_identifier(prec,email)                        
                        
                        if has_email_ident:
                            # LOG and continue -> no need to update, identifier already there
                            logger.info(f"EMAIL IDENTIFIER EXISTS: Barcode({barcode}), Email({email})")
                            continue

                        if show_list and (not has_email_ident):
                            # build a file of the records that will have the email added as an identifier  
                            # build list now, gen file after
                            upd_rec = {"barcode":barcode,"email":email}     
                            email_list.append(upd_rec)                            
                            record_count = record_count + 1
                        else:      
                            upd_result = update_identifier(barcode, prec,email)
                            if upd_result:
                                # SUCCESS
                                logger.info(f"UPDATE SUCCESS for barcode: {barcode} and email: {email}")
                                record_count = record_count + 1
                            else:
                                # LOG FAILURE    
                                failed_count = failed_count + 1                        
                                logger.info(f"UPDATE FAILED for barcode: {barcode} and email: {email}")
                        
                        # wait a little to ensure we don't do more than 25 api calls in 1 sec
                        # we're running single threaded                        
                        time.sleep(0.04)
                    else:
                        logger.info(f"EMPTY EMAIL")
                else:
                    logger.info(f"EMPTY BARCODE")
        
        # gen the show / tmp file, how many things would we update?
        if show_list:
            with open(tmp_path, "a") as tmpfile:
                for rec in email_list:
                    barcode = rec.get("barcode", "")
                    email = rec.get("email", "")
                    tmpfile.write(f"{barcode},{email}\n")
    except Exception as e:
        exception_logger.exception(e)
    
    return record_count, failed_count

if __name__ == "__main__":
    '''
    A script to process csv files that UG generates of barcode / email to update ALMA records with the email as an identifier.
    The email as an identifier will allow for authentication into system
    This only needs to be run once POST MIGRATE
    '''    
    dt_end = None
    dt_start = None    
    eastern=pytz.timezone('America/Toronto')

    try:
        logger.info("********** RUNNING THE ALMA EMAIL IDENTIFIER UPDATE **********")                     
        dt_start = datetime.now(tz=eastern)           
        logger.info("sys args: " + str(sys.argv))

        # for each csv file in the csv_input directory, process line by line
        # and look up user, and update they record with the email identifier
        
        record_count = 0        
        total_record_count = 0                
        total_failed_count = 0                
        csv_file_count = 0

        # are we updating or skipping updates and just generating a list
        show_list = False

        if len(sys.argv) > 1:
            func = sys.argv[1].split(' ')[0]
            if func == "list":
                show_list = True            
                logger.info("NO ALMA UPDATES WILL BE DONE -> GENERATING RECORD LIST THAT WOULD BE IMPACTED")
        
        csv_path = f"{BASE_DIR}{os.sep}csv_input{os.sep}"

        # if we're showing a list, use tmp to show those recs that would updated
        csv_tmp_output = f"{BASE_DIR}{os.sep}csv_tmp_output{os.sep}" 
            
        if os.path.isdir(csv_path):
            # dir is found, get the csv files located in it
            dir_list = os.listdir(csv_path)
            for fname in dir_list:            
                if ".csv" in fname:
                    csv_file_count = csv_file_count + 1  
                    logger.info(f"Processing csv file: {csv_path}{fname}")
                    record_count, failed_count = process_file(csv_path,fname,show_list,csv_tmp_output)
                    if show_list:
                        logger.info(f"{record_count} Alma user records WILL BE updated from file {fname}")                            
                    else:                                
                        logger.info(f"{record_count} Alma user records updated from file {fname}")
                        logger.info(f"{failed_count} Alma user records FAILED THE UPDATE from file {fname}")

                    total_record_count = total_record_count + record_count                                            
                    total_failed_count = total_failed_count + failed_count                                            
        else:           
            logger.info("CSV input path not found")
        
        if show_list:
            logger.info(f"{total_record_count} TOTAL Alma user records WILL BE updated from all csv input files")                        
        else:            
            logger.info(f"{total_record_count} TOTAL Alma user records updated from all csv input files")
            logger.info(f"{total_failed_count} TOTAL Alma user records FAILED THE UPDATE from all csv input files")

        dt_end = datetime.now(tz=eastern)

        #determine the time delta from start to finish
        delta = (dt_end - dt_start).total_seconds()                                             
    
        logger.info(f"Number of csv files processed: {csv_file_count}")
        logger.info(f"********** ALMA EMAIL IDENTIFIER UPDATE PROCESS OVER AT: {dt_end} **********")        

    except Exception as e:
        exception_logger.exception(e)


