# Alma community email update via Alma API

## Situation
A script to process csv files that UG generates of barcode / email to update ALMA records with the email as an identifier.
The email as an identifier will allow for authentication into system. This only needs to be run once POST MIGRATE

Once Alma data migration is complete, use the analytics to create a report for Alumni / Community and SSO users with BARCODE,EMAIL
The email must be moved into the identifiers field for account authentication.

## How to run / test

### General info
Since this is a one time job you could run from anywhere, and probably don't want to even put on a server.

This repository / job can be run as a docker container, you can install docker desktop from here: https://www.docker.com/products/docker-desktop

this way, you don't need to know how to setup a python environment. Once docker is installed, ensure it's running.

This repository/project was built using Visual Studio Code and uses the remote-containers extension. Using this, you could debug the script
if you need to make changes. details on this are located with the VSC_README.md README file and more details here: https://devblogs.microsoft.com/python/remote-python-development-in-visual-studio-code/

### Setup

1. Download or clone the repository
2. copy the sample.ini file and create alma_email_ident.ini in the same root location (where the .py files exist)
3. in alma_email_ident.ini place your Alma api_host and ALma users_api_key info under the [api] section.
4. place all your csv migration files from Ex Libris in the csv_input directory. The script will process all csv files located in this dir.

### Actual commands
You could use docker compose to run this if familiar, I'll include those simplier instructions at the end, but I'll provide more detailed instruction with the following explict commands to make it a little more clear.

#### detailed commands
In the run commands, we must specify an absolute path to the logs directory and the csv_tmp_output directory, to ensure we have the log files after the container job finishes running, and we can easily view if desired.
The commands below will be: docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-alma-email-docker-dev-update
where you must change FULL_PATH to your project/repository logs folder. 

eg: docker run -v /Users/zeus/gitdev/alma-community-email-update/logs:/code/logs -v /Users/zeus/gitdev/alma-community-email-update/csv_tmp_output:/code/csv_tmp_output --rm py-alma-email-docker-dev-update

1. open a terminal or command prompt and change to the repository directory (where the Dockerfile is located)
2. docker build -t py-alma-email-docker-dev-tests . -f Dockerfile.tests
3. That will build the test container. We'll run this to ensure all setup critera is met (alma_email_ident.ini exists, csv files in place)
4. docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-alma-email-docker-dev-tests
    eg. docker run -v /Users/zeus/gitdev/alma-community-email-update/logs:/code/logs -v /Users/zeus/gitdev/alma-community-email-update/csv_tmp_output:/code/csv_tmp_output --rm py-alma-email-docker-dev-tests
5. That will run the tests, you'll want to see that 3 tests passed, output should be this:

alma-community-email-update » docker run -v /Users/zeus/gitdev/alma-community-email-update/logs:/code/logs -v /Users/zeus/gitdev/alma-community-email-update/csv_tmp_output:/code/csv_tmp_output --rm py-alma-email-docker-dev-tests  ~/gitdev/alma-community-email-update
============================= test session starts ==============================
platform linux -- Python 3.7.4, pytest-7.3.1, pluggy-1.0.0
rootdir: /code
collected 3 items

test_alma_email.py ...                                                   [100%]

============================== 3 passed in 0.01s ===============================

6. docker build -t py-alma-email-docker-dev-gen-lists . -f Dockerfile.list_impacted
7. that will build the container that will generate the list, see how many records we'll be impacting.
8. docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-alma-email-docker-dev-gen-lists
    docker run -v /Users/zeus/gitdev/alma-community-email-update/logs:/code/logs -v /Users/zeus/gitdev/alma-community-email-update/csv_tmp_output:/code/csv_tmp_output --rm py-alma-email-docker-dev-gen-lists
9. You can look at the logs and csv_tmp_output to review records.
10. docker build -t py-alma-email-docker-dev-update .
11. that will build the container that will perform the actual update
12. You may want to backup the log files / generated list, the update will overwrite / cause these log files to roll over
13. looks good, time to perform the update. This could take a long time
14. docker run -v [FULL_PATH]:/code/logs -v [FULL_PATH]:/code/csv_tmp_output --rm py-alma-email-docker-dev-update
15. All records should be done

#### Docker Compose commands

1. in the docker-compose.yml and the docker-compose-update.yml files changes the paths under the 'volume' section to match your environment.
2. in the comamnd line run the following to generate the log and review list: docker-compose up --build
3. to perform the update: docker-compose -f docker-compose-update.yml up --build
4. Both will perform the tests before, so you can see if the setup is correct

## clean up
we want to remove unneeded images and tidy up after our process

docker-compose rm
docker-compose -f docker-compose-update.yml rm