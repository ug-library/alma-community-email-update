import os
import pytest
import configparser

BASE_DIR = os.path.dirname(os.path.realpath(__file__))

def test_alma_data_files():
    """
    ensure the datafiles are seen within container, checking the csv_input directory
    """
    result = False
    
    fpath = f"{BASE_DIR}{os.sep}csv_input{os.sep}"    
    try:       
        if os.path.isdir(fpath):
            # dir is found, get the csv files located in it
            dir_list = os.listdir(fpath)
            for fname in dir_list:            
                if ".csv" in fname:
                    # found one, we can stop
                    result = True
                    break                
    except Exception:
        pass    

    assert result, "No csv files located in csv_input directory"

def test_config_file_exists():
    """
    ensure the ini config file exists and is available within container, same location as this file
    """
    result = False
    
    fpath = f"{BASE_DIR}{os.sep}alma_email_ident.ini"
    try:        
        if os.path.exists(fpath):
            result = True                
    except Exception:
        pass    

    assert result, f"config file alma_email_ident.ini not found in current directory: {BASE_DIR}"

def test_config_file_contents():
    """
    ensure the ini config file has the appropriate keys in place same location as this file
    """
    result = True
        
    try:      
        fpath = f"{BASE_DIR}{os.sep}alma_email_ident.ini"
        config = configparser.ConfigParser()
        config.read(fpath)

        config['api']['users_api_key']                
            
    except Exception:
        result = False        

    assert result, "[api] setup for users_api_key not found in alma_email_ident.ini config file"
